#pragma once
#include "Investment.h"

class ListItem
{	
	private:
		Investment value;
		ListItem* next;
public:
	ListItem(Investment value, ListItem* next) {
		this->value = value;
		this->next = next;
	}

	Investment getValue() {
		return value;
	}

	ListItem* getNext() {
		return next;
	}
	
	ListItem();
	~ListItem();
};
