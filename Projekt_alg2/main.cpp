#include "List.h"
#include "Investment.h"

using namespace std;
int main() {
	List* list = new List();

	Investment* a = new Investment(1, 1, 1);
	Investment* b = new Investment(2, 2, 2);
	Investment* c = new Investment(3, 3, 3);

	list->insert(a);
	list->insert(b);
	list->insert(c);
	do {
		Investment* d;
		d = &list->getCurrentValue();
		d->print();
		d->setInterestRate(510);
		//cout << boolalpha << list->update(d);
	} while (list->moveNext());

	cout << endl << endl;

	list->reset();
	do{
		Investment *d;
		d = &list->getCurrentValue();
		d->print();
	} while (list->moveNext());

	Investment* d = new Investment(5000, 2, 500);
	return 1;
}