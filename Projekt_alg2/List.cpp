#include "List.h"

#ifndef LIST
#define LIST

//podivat se na to zacykleni
/*bool List::isInList(Investment* value)
{
	ListItem* start = this->current;
	do {
		if (this->current->value->getIdentification() == value->getIdentification()) {
			return true;
		}
		moveNext();
	} while(start != this->current);
	return false;
}*/

List::ListItem* List::searchItemById(int id)
{
	ListItem* start = this->head;
	while (start != nullptr) {
		if (start->value->getIdentification() == id) {
			return start;
		}
		start = start->next;
	}
	return nullptr;
}

List::List()
{
	this->head = nullptr;
	this->tail = nullptr;
	this->current = nullptr;
}


List::~List()
{
	reset();
	ListItem* next = nullptr;
	do {
		ListItem* next = this->current->next;
		delete this->current;
	} while (next->next == nullptr);
	delete next;
}

void List::insert(Investment* value)
{
	ListItem* item = new ListItem;
	item->value = value;
	item->next = nullptr;
	if (!this->isEmpty()) {
		item->next = this->head;
	}
	else {
		this->tail = item;
	}
	this->head = this->current = item;
}

//dodelat
bool List::remove(Investment* value)
{
	ListItem* searchetItem = searchItemById(value->getIdentification());
	if(searchetItem == nullptr)
		return false;
	return true;
}

bool List::update(Investment* value)
{
	ListItem* item = this->searchItemById(value->getIdentification());
	if (item == nullptr) {
		return false;
	}
	item->value = value;
	return true;
}

bool List::isEmpty()
{
	return (this->head == nullptr);
}

bool List::isEnd()
{
	return (this->current->next == nullptr);
}

bool List::moveNext()
{
	if (this->current->next != nullptr) {
		this->current = this->current->next;
		return true;
	}
	return false;
}

void List::reset()
{
	this->current = this->head;
}

Investment List::getCurrentValue()
{
	if (this->current != nullptr) {
		return *(this->current->value);
	}
}
#endif