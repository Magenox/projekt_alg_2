#pragma once
class Investment
{
private:
	static int NOCreatedInvestments; //zde si pocitam pcoet vsech vytvorenych investic (pro generovani prislusnych idcek)

	const int identification;
	
	double initialDeposit;
	int initInterestRate;
	int interestRate;
	double balance;
	double monthlyFee;

public:
	Investment(double initialDeposit, int initInterestRate, double monthlyFee);
	Investment();
	~Investment();

	int getIdentification();
	double getBalance(); //aktualni stav penez
	double getInitialDeposit();
	int getInitInterestRate();
	int getInterestRate();
	double getMonthlyFee();

	void setInterestRate(int interestRate);

	void deposit(double amount); //vlozit
	double withdraw(double amount);	//vybrat
	void conversion(); //prepocet poplatku a urokove sazby (bude se pocitat za kazdy mesic)
	
	void print();
};

