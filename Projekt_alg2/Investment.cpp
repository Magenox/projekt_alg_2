#include "Investment.h"
#include <iostream>

using namespace std;

int Investment::NOCreatedInvestments = 0;

Investment::Investment(double initialDeposit, int initInterestRate, double monthlyFee) : identification(Investment::NOCreatedInvestments + 1)
{
	Investment::NOCreatedInvestments += 1;
	this->initialDeposit = this->balance = initialDeposit;
	this->initInterestRate = this->interestRate = initInterestRate;
	this->monthlyFee = monthlyFee;
}

Investment::Investment() : identification(Investment::NOCreatedInvestments + 1)
{
}


Investment::~Investment()
{
}

int Investment::getIdentification()
{
	return this->identification;
}

double Investment::getBalance()
{
	return this->balance;
}

double Investment::getInitialDeposit()
{
	return this->initialDeposit;
}

int Investment::getInitInterestRate()
{
	return this->initInterestRate;
}

int Investment::getInterestRate()
{
	return this->interestRate;
}

double Investment::getMonthlyFee()
{
	return this->monthlyFee;
}

void Investment::setInterestRate(int interestRate)
{
	this->interestRate = interestRate;
}

void Investment::deposit(double amount)
{
	this->balance += amount;
}

double Investment::withdraw(double amount)
{
	this->balance -= amount;
	return amount;
}

void Investment::conversion()
{
}

void Investment::print()
{
	cout << "Identifikator: " << this->identification << endl;
	cout << "Penize v investici: " << this->balance << endl;
	cout << "Pocatecni vklad: " << this->initialDeposit << endl;
	cout << "Urokova sazba: " << this->interestRate << endl;
	cout << "Pocatecni urokova sazba: " << this->initInterestRate << endl;
	cout << "Mesicni poplatek za sluzby: " << this->monthlyFee << endl;
	cout << endl << endl;
}


