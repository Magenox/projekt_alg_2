#pragma once
#include "Investment.h"
#include <iostream>

class List
{
	private:

		struct ListItem{
			Investment* value;
			ListItem* next;
		};

		ListItem* current;
		ListItem* head;
		ListItem* tail;

		ListItem* searchItemById(int id);

	public:
		List();
		~List();

		void insert(Investment* value);
		bool remove(Investment* value);
		bool update(Investment* value);

		//bool isInList(Investment* value);

		bool isEmpty();
		bool isEnd();
		bool moveNext();

		void reset();

		Investment getCurrentValue();
};

/*class ListItem {
private:
Investment value;
ListItem* next;
public:
ListItem(ListItem* next) {
this->value = value;
this->next = next;
}

Investment getValue() {
return value;
}

ListItem* getNext() {
return next;
}
};*/